class AttackObject extends BaseWorldObject
{
	constructor(game, options) 
	{
		super(game, options);
	    this.owner = options.owner;
	    this.isAttack = true;
	    this.damage = 1;
	    this.coolDown = 1000;
	    this.nextFire = 0;	    
	}

	isCooledDown()
	{
	    return game.time.now > this.nextFire && this.sprite.countDead() > 0;
	}

	heatUp() 
	{
        this.nextFire = game.time.now + this.coolDown;
	}

	update(layer) 
	{

	}

}