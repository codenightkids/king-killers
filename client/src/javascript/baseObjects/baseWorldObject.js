class BaseWorldObject 
{
    constructor(game, options) 
    {
        this.sprite = null;
        this.game = game;
        this.options = options;
        this.inventory = [];
        this.movePoint = [NaN, NaN];
        this.previousMovePoint = [NaN, NaN];
        this.id = options.id || null;
        worldObjects.push(this);
        this.serverSampleRate = 4/60;

        var obj = {'classNane':this.constructor.name, "test":"test"};
        arenas.emit('addObject', obj, (result)=>{
            this.id = result.id;
            this.setup(game);
        });
    }

    useItem(params) {
        var itemIndex = params.index;
        var targetId = params.target;


        var targetIdx = worldObjects.findIndex((obj)=>{
            console.log(obj.id, targetId);
            return obj.id == targetId;
        });

        this.item = this.inventory[itemIndex];

        if(this.item === undefined) 
        {
            return;
        }

        if(this.item.canUse())
        {
            this.item.activate(worldObjects[targetIdx]);
            if(this.item.charges <= 0)
            {
                this.inventory.splice(itemIndex, 1);
            }
        }
    }

    addToInventory(item)
    {
        this.inventory.push(item);
        item.sprite.exists = false;
    }

    dropInventory()
    {
        var numItems = this.inventory.length;
        for(var i = 0; i < numItems; i++)
        {
            var item = this.inventory.pop();
            item.sprite.x = this.sprite.x;
            item.sprite.y = this.sprite.y;
            item.sprite.exists = true;
        }
    }

    changedProperties() //todo: track more properties to include in deltas
    {
        var delta = {};
        if(!isNaN(this.movePoint[0]) && this.movePoint[0] != this.previousMovePoint[0] && this.movePoint[1] != this.previousMovePoint[1]) 
        {
            delta.movePoint = this.movePoint;
            this.previousMovePoint = this.movePoint;
        }

        var propKeys = [];
        for(var k in delta) propKeys.push(k);

        if( propKeys.length == 0 ) {
            return false;
        } else {
            return delta;
        }
    }

    objectChange(data)
    {
        arenas.emit('objectUpdate', {
            'objectId': this.id,
            'data': data
        });
    }

    deserializeDeltaData(deltas)
    {
        // console.log('deserlizeDelta');
        for(var key in deltas.data)
        {
            // console.log('deserlizeDelta', key);
            var ns = key.split('.');


            switch (ns.length) {
                case 1:
                    if(this[ns[0]])
                    {
                        if(typeof(this[ns[0]]) === 'function') 
                        {
                            this[ns[0]](deltas.data[key]);
                        } 
                        else 
                        {
                            this[ns[0]] = deltas.data[key];
                        }
                    }
                    break;
                case 2:
                    if(this[ns[0]][ns[1]])
                    {
                        if(typeof(this[ns[0]][ns[1]]) === 'function') 
                        {
                            this[ns[0]][ns[1]](deltas.data[key]);
                        } 
                        else 
                        {
                            this[ns[0]][ns[1]] = deltas.data[key];
                        }
                    }
                    break;
                case 3:
                    if(this[ns[0]][ns[1]][ns[2]])
                    {
                        if(typeof(this[ns[0]][ns[1]][ns[2]]) === 'function') 
                        {
                            this[ns[0]][ns[1]][ns[2]](deltas.data[key]);
                        } 
                        else 
                        {
                            this[ns[0]][ns[1]][ns[2]] = deltas.data[key];
                        }
                    }
                    break;
            }
        }
    }

    setup(game) 
    {
    }

    collide(collidedObject) 
    {

    }

    update() 
    {

    }

    render()
    {

    }
    
}