class ItemObject extends BaseWorldObject 
{
    constructor(game, options)
    {
        super(game, options);
        this.isItem = true;
        this.charges = 1;
    }

    canUse(options)
    {

    }
}