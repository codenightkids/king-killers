class HealthPot extends ItemObject
{
    constructor(game, options) 
    {
        super(game, options);
        this.sprite = game.add.sprite(80, 80, 'healthPot', 0);
        game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
        this.sprite.exists = options.exists || false;
        console.log(this.sprite.exists);
    }

    activate(targetObject)
    {
        targetObject.health = 100;
        var oldSpeed = targetObject.movementSpeed;
        targetObject.movementSpeed = 120;
        this.charges = 0;
        setTimeout(function(){
            targetObject.movementSpeed = oldSpeed;
        },5000);
    }

    canUse()
    {
        return true;
    }
}