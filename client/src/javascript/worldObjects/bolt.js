class Bolt extends AttackObject
{
	constructor(game, options) 
	{
		super(game, options);
	    
	    this.sprite = game.add.group(); //sprite group
	    this.sprite.enableBody = true;
	    this.sprite.physicsBodyType = Phaser.Physics.ARCADE;
	    this.sprite.createMultiple(5, 'orb'); // limit 5 on the screen at a time
	    
		this.damage = 5;
	    this.coolDown = 500; 
	    this.speed = 300;
	}

	fireFromTo(paramsArray)
	{
		var startPosition = paramsArray[0];
		var toPosition = paramsArray[1];
	    if (this.isCooledDown() && this.owner.sprite.alive)
	    {
	       this.movePoint = [toPosition.x, toPosition.y];
            var bullet = this.sprite.getFirstDead();
	        bullet.reset(startPosition.x + 3, startPosition.y + 3);
            game.physics.arcade.moveToXY(bullet, toPosition.x, toPosition.y, this.speed)
	        this.heatUp(); //opposite of cool down :)
	    }
	}

  update(layer) {
    // kill an invidual bolt sprite when collides with tilemap (aka. layer)
    game.physics.arcade.collide(this.sprite, layer, (aBolt)=>{
    	aBolt.kill();
    });
  }

}