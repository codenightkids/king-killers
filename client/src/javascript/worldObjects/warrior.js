class Warrior extends Player {
    
  constructor(game, options) {
    super(game, options);

    this.movementSpeed = 80;
    this.health = 150;

    this.attacks['q'] = new Bolt(game, {owner: this});
    this.attacks['w'] = new Bolt(game, {owner: this});
    this.attacks['e'] = new Bolt(game, {owner: this});
    this.attacks['r'] = new Bolt(game, {owner: this});
    this.attacks['spacebar'] = new Bolt(game, {owner: this});

    this.inventory.push(new HealthPot(game, {exists: false}));

        setTimeout(()=>{
            console.log("addObject");
            arenas.emit('addObject', this.id);
        },100);

  }

  attackRequest(type) {
    if(!this.sprite.alive) {
        return
    }

    let mouseX = game.input.mousePointer.x + game.camera.x;
    let mouseY = game.input.mousePointer.y + game.camera.y;

    if(type == 'q') {

        this.objectChange({
            'attacks.q.fireFromTo': [
                {x: this.sprite.x, y:this.sprite.y},
                {x: mouseX, y:mouseY}
            ]
        });
    }
    if(type == 'w') {
        this.objectChange({
            'attacks.w.fireFromTo': [
                {x: this.sprite.x, y:this.sprite.y},
                {x: mouseX, y:mouseY}
            ]
        });
    }
    if(type == 'e') {
        this.objectChange({
            'attacks.e.fireFromTo': [
                {x: this.sprite.x, y:this.sprite.y},
                {x: mouseX, y:mouseY}
            ]
        });
    }
    if(type == 'r') {
        this.objectChange({
            'attacks.r.fireFromTo': [
                {x: this.sprite.x, y:this.sprite.y},
                {x: mouseX, y:mouseY}
            ]
        });
    }
    if(type == 'spacebar') {
        this.objectChange({
            'attacks.spacebar.fireFromTo': [
                {x: this.sprite.x, y:this.sprite.y},
                {x: mouseX, y:mouseY}
            ]
        });
    }
  }

  update(layer)
  {
    super.update(layer);
    // if(!this.options.localPlayer) {
    //     this.demoAnimateDumbBot();
    // }
  }

  demoAnimateDumbBot() 
  {
    if(!this.sprite.alive) {
        return
    }
    var randomDirection = Math.floor(Math.random() * 4);
    var velocity = 100;
    var directions = [['x', 'left', -1], ['x','right', 1], ['y','up', 1], ['y','down', -1]];
    var direction = directions[randomDirection];
    this.sprite.body.velocity[direction[0]] = velocity * [direction[2]];
    this.sprite.play(direction[1]);

    this.attacks['q'].fireFromTo({x: this.sprite.x, y:this.sprite.y}, {x: Math.floor(Math.random() * 1000), y: Math.floor(Math.random() * 1000)});
  }

}