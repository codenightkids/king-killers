class Player extends BaseWorldObject {
  
  constructor(game, options) {
    super(game, options);

    this.movementSpeed = 80;
    this.health = 100;
    this.attacks = {
        'q': null,
        'w': null,
        'e': null,
        'r': null,
        'spacebar':null
    }
    this.scale = 0.33;
  }
  
  setup(game) {
    // setup the sprite
    var scale = 0.33;
    var spawnPoint = {x: 48, y: 48};
    this.sprite = game.add.sprite(spawnPoint.x, spawnPoint.y, 'player', 'deathknight [www.imagesplitter.net]-0-0');
    this.sprite.animations.add('walk_right',  Phaser.Animation.generateFrameNames('deathknight [www.imagesplitter.net]-1-', 0, 3, '', 1), 8, true, false);
    this.sprite.animations.add('walk_down',  Phaser.Animation.generateFrameNames('deathknight [www.imagesplitter.net]-7-', 0, 3, '', 1), 8, true, false);
    this.sprite.animations.add('walk_up',  Phaser.Animation.generateFrameNames('deathknight [www.imagesplitter.net]-4-', 0, 3, '', 1), 8, true, false);

    this.sprite.scale.setTo(scale, scale);
    this.sprite.anchor.setTo(.5,.5);

    // Give the player a body, and set the size
    game.physics.enable(this.sprite, Phaser.Physics.ARCADE); 
    this.sprite.body.setSize(62, 82, 30, 10);

    if(this.options.localPlayer) {
        game.camera.follow(this.sprite);

        qKeyboardKey.onDown.add(()=>{
            this.attackRequest('q');
        });
    
        wKeyboardKey.onDown.add(()=>{
            this.attackRequest('w');
        });

        eKeyboardKey.onDown.add(()=>{
            this.attackRequest('e');
        });

        rKeyboardKey.onDown.add(()=>{
            this.attackRequest('r');
        });

        spaceBarKeyboardKey.onDown.add(()=>{
            this.attackRequest('spacebar');
        });

        num1KeyboardKey.onDown.add(()=>{
            this.objectChange({
                'useItem': {index: 0, target: this.id}
            });
            // this.useItem(0)
        });

    }
  }

  collide(object) {
    // console.log('collide', object, this);
    if(object.isAttack && object.owner && object.owner != this) {
        this.getAttacked(object);
    }
    if(object.isItem) {
        this.addToInventory(object);
    }
  }

  getAttacked(attackObject) {
    this.health -= attackObject.damage;
  }

  lifeCheck() {
    if(!this.sprite.alive) {
        return;
    }

    if(this.health > 50) {
        this.sprite.tint = 0xffffff;
    }

    if(this.health <= 50) {
        this.sprite.tint = 0x9999ff;
    }

    if(this.health <= 25) {
        this.sprite.tint = 0xff9999;
    }

    if(this.health <= 0) {
        this.kill();
    }
  }

  kill() 
  {
    this.dropInventory();
    this.sprite.kill();
  }

  attackRequest(type) {
    // this should be overwritten by child classes
  }

  setNewPosition(x, y) {
    this.objectChange({
        'movePoint': [x, y]
    });
  }

  update(layer) {
    if(this.sprite == undefined)
        return false;

    this.lifeCheck();
    game.physics.arcade.collide(this.sprite, layer);
    // If this is a local player, track mouse pointer.
    if(this.options.localPlayer) 
    {
        // if right button pressed, set a move point to go to.
        if(game.input.mousePointer.rightButton.isDown) 
        {
            let x = game.input.mousePointer.x + game.camera.x;
            let y = game.input.mousePointer.y + game.camera.y;
            this.setNewPosition(x, y);
        }
    } 

    // move the character to it's move point.
    this.sprite.body.velocity.set(0);

    var xShouldMove = Math.abs(Math.abs(this.sprite.x) - Math.abs(this.movePoint[0])) > this.movementSpeed / 60;
    var yShouldMove =  Math.abs(Math.abs(this.sprite.y) - Math.abs(this.movePoint[1])) > this.movementSpeed / 60;

    var xAbsDist = Math.abs(Math.abs(this.sprite.x) - Math.abs(this.movePoint[0]));
    var xMovementOffset = xAbsDist < 100 ? xMovementOffset : 0;

    if(this.sprite.x < this.movePoint[0] && xShouldMove) 
    {
        this.sprite.body.velocity.x += this.movementSpeed;
    }

    if(this.sprite.x > this.movePoint[0] && xShouldMove) 
    {
        this.sprite.body.velocity.x -= this.movementSpeed;
    }

    if(this.sprite.y < this.movePoint[1] && yShouldMove) 
    {
        this.sprite.body.velocity.y += this.movementSpeed;
    }

    if(this.sprite.y > this.movePoint[1] && yShouldMove) 
    {
        this.sprite.body.velocity.y -= this.movementSpeed;
    }

    // console.log(this.sprite.body.velocity, this.movementSpeed)

    // determine what sprite to animate for the character
    if(this.sprite.body.velocity.x > 1 && this.sprite.body.velocity.y == 0)
    {
        this.sprite.play('walk_right')
        this.sprite.scale.x = this.scale * 1;
    }

    if(this.sprite.body.velocity.x < -1 && this.sprite.body.velocity.y == 0)
    {
        this.sprite.play('walk_right');
        this.sprite.scale.x = this.scale * -1;
    }

    if(this.sprite.body.velocity.y > 1) 
    {
        this.sprite.play('walk_down');
    }

    if(this.sprite.body.velocity.y < -1) 
    {
        this.sprite.play('walk_up');
    }

    if(!yShouldMove && !xShouldMove) 
    {
        this.sprite.animations.stop();
    }

  }

  render()
  {
    // game.debug.body(this.sprite);        
  }
}