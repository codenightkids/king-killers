// globals
var vueApp = null;
var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'game-container');
var map;
var layer;
var cursors;
var worldObjects = [];
var serverRoomData = null;
var prevServerRoomData = null;
var activeGameRooms = null;
var activeRoomID = null;
var sessionID = null
var arenas = io.connect('http://localhost:5374/arenas')

arenas.on('connect', function () {
    sessionID = arenas.id;
});

arenas.on('openRooms', function (data) {
    console.log("openRooms", data);
    activeGameRooms = data.games;
});    

// 
arenas.on('objectUpdate', (data)=>{
    // console.log('objectUpdate', data);
    arenaState.updateObjects(data);
});

arenas.on('roomUpdate', (data)=>{
    prevServerRoomData = serverRoomData;
    serverRoomData = data;
    arenaState.updateUsers();
})

var bootState = {
    preload: function() {
        game.load.tilemap('map', 'assets/tilemaps/arena-1.csv', null, Phaser.Tilemap.CSV);
        game.load.image('tiles', 'assets/tilemaps/arena-1.png');
        game.load.image('orb', 'assets/images/purple_ball.png', 16, 16);
        game.load.image('healthPot', 'assets/images/heart.png', 16, 16);
        game.load.atlasJSONHash('player', 'assets/images/deathknight-tp.png', 'assets/images/deathknight-tp.json');
        game.canvas.oncontextmenu = function() {
             return false;  
        } 
    },

    create: function() {
        // start the arena
        console.log("Game started");
        game.state.start('connect');
    }
}

var connectState = {
    create: function() 
    {
        // create connection setup
        vueApp = new Vue({
          el: '#hud',
          data: {
            activeGames: activeGameRooms,
            inRoom: false
          },
          methods: {
            joinRoom: function(room) {
                arenas.emit('joinRoom', {room: room}, function(joined){
                    if(joined) 
                    {
                        vueApp.inRoom = true;
                        game.state.start('arena');
                    }
                });
            }
          }
        })
        console.log("TODO: Game Connected")
    }
}

var arenaState = {

    created: false, 
    create: function() 
    {
        console.log("Arena Started")
        //  Because we're loading CSV map data we have to specify the tile size here or we can't render it
        map = game.add.tilemap('map', 16, 16);

        //  Now add in the tileset
        map.addTilesetImage('tiles');
        
        //  Create our layer
        layer = map.createLayer(0);

        //  Resize the world
        layer.resizeWorld();

        //  This isn't totally accurate, but it'll do for now
        map.setCollisionBetween(5, 8);
        map.setCollisionBetween(16, 24);
        map.setCollisionBetween(38, 39);
        map.setCollisionBetween(54, 55);
        map.setCollisionBetween(70, 70);
        map.setCollisionBetween(86, 86);
        map.setCollisionBetween(102, 102);
        map.setCollisionBetween(73, 73);
        map.setCollisionBetween(89, 89);
        map.setCollisionBetween(105, 107);
        map.setCollisionBetween(116, 118);



        qKeyboardKey = this.game.input.keyboard.addKey(Phaser.Keyboard.Q);
        wKeyboardKey = this.game.input.keyboard.addKey(Phaser.Keyboard.W);
        eKeyboardKey = this.game.input.keyboard.addKey(Phaser.Keyboard.E);
        rKeyboardKey = this.game.input.keyboard.addKey(Phaser.Keyboard.R);
        spaceBarKeyboardKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        num1KeyboardKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        cursors = game.input.keyboard.createCursorKeys();

        this.created = true;
        this.updateUsers();
        game.stage.disableVisibilityChange = true;
    },

    updateUsers: function() 
    {

        console.log('updateUsers', this.created);
        var users = serverRoomData.users;
        var prevUsers = prevServerRoomData != null? prevServerRoomData.users : [];

        if(!this.created) { return false; }
        // do they exist already? Don't worry about it!
        // Do they not exist in the prev and new roomdata? Add them.
        // do they exist in prevRoomData, but not exist in the new roomdata? Remove them
    
        var newUsers = users.filter((subUser)=>{
            var found = prevUsers.find((prevSubUser)=>{
                return subUser.id == prevSubUser.id;
            }) 
            return found == undefined; // if we can't find the user, it's new.
        });

        var removeUsers = prevUsers.filter((prevSubUser)=>{
            return users.find((subUser)=>{
                return subUser.id == prevSubUser.id;
            }) == undefined; // if we can't find the user, it's old.
        });

        // add users
        newUsers.forEach((user)=>{
            console.log(user.id, sessionID)
            if(user.id == sessionID) 
            {
                new Warrior(game, {id: user.id, localPlayer: true});
            } else {
                new Warrior(game, {id: user.id, localPlayer: false});
            }
        });

        // remove users
        removeUsers.forEach((user)=>{
            worldObjects.forEach((worldObject, index)=>{
                if(worldObject.id == user.id) 
                {
                    worldObject.sprite.destroy();
                    worldObjects.splice(index, 1);
                }
            });
        });
    },

    updateObjects: function(deltaData)
    {
        var objIdx = worldObjects.findIndex((obj)=>{
            // console.log('findIndex', deltaData, obj);
            return obj.id == deltaData.objectId;
        });

        worldObjects[objIdx].deserializeDeltaData(deltaData);
    },

    update: function() 
    {
        worldObjects.forEach((object)=>{
            // Collide with everything
            worldObjects.forEach((subObject)=>{
                game.physics.arcade.overlap(object.sprite, subObject.sprite, ()=>{ object.collide(subObject) }, null, this);
            });
            // Update for layer checking
            object.update(layer);
        });
    },

    render: function() 
    {
        worldObjects.forEach((object)=>{
            // Collide with everything
            worldObjects.forEach((subObject)=>{
                game.physics.arcade.overlap(object.sprite, subObject.sprite, ()=>{ object.collide(subObject) }, null, this);
            });
            // Update for layer checking
            object.render(layer);
        });       
    }
}

// game states
game.state.add('boot', bootState);
game.state.add('connect', connectState);
game.state.add('arena', arenaState);
// start the game!
game.state.start('boot');
