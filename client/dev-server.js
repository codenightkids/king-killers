let express = require('express');
let gulp = require('gulp');
let livereload = require('gulp-livereload');
let path = require('path');

gulp.task('dev-watch', ()=>{
    var app = express();
    app.set('port', 3001);
    app.use(express.static(path.join(__dirname, 'src')));
    app.listen(app.get('port'), function() {
      console.log('Magic happens on port 3001');
    });

    livereload.listen();
    gulp.watch(['src/**/*.*', 'src/*.*'], ['file-change']);
});

gulp.task('file-change', ['jslint'], ()=>{
    console.log("file-change");
    livereload.reload();
});

gulp.task('jslint', ()=>{
//implement todo
});

var taskToSTart = process.argv[2] ? process.argv[2] : "dev-watch";
gulp.start(taskToSTart);
