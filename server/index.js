let io = require('socket.io')(5374);
let uuidv1 = require('uuid/v1');
let activeGameRooms = [];
let activeUsers = [];

ACTIVITIES = {
    LOBBY: 0,
    INGAME: 1
}

class worldObject {
    constructor() {
        this.id = uuidv1();
        this.x = null;
        this.y = null;
        this.objectClassName = null;
        this.health = null;
        this.alive = null;
        this.exists = null;
    }

    setProperties(props) {
        for(var k in props) 
        {
            this[k] = props[k];
        }
    }

}

class User {
    constructor() {
        this.id = null;
        this.playerName = null;
        this.activity = null;
        this.roomId = null;
        this._room = null;
    }
}

class Room {
    constructor() 
    {
        this.users = [];
        this.id = null;
        this.worldObjects = [];
    }
    
    setID(id) 
    {
        this.id = id;
    }
    
    addUser(user) 
    {
        this.users.push(user);
    }

    emitUpdate(socket = null) 
    {
        if(socket != null) {
            socket.to(this.id).emit('roomUpdate', this);
        } else {
            io.of('arenas').in(this.id).emit('roomUpdate', this);
        }
    }

    removeUser(user)
    {
        this.users = this.users.filter((subUser)=>{
            return user.id != subUser.id; 
        })
        // this.users.splice(roomUserIdx, 1);
    }

    getWorldObject(worldObject)
    {
        return this.worldObjects = this.worldObjects.filter((obj)=>{
            return worldObject.id == obj.id;
        })[0];
    }

    addWorldObject(worldObjectProperties)
    {
        worldObjectProperties.id = uuidv1();

        var newWorldObject = new worldObject();
        newWorldObject.setProperties(worldObjectProperties);

        this.worldObjects.push(worldObject);
        return newWorldObject;
    }

    removeWorldObject(worldObject)
    {
        this.worldObjects = this.worldObjects.filter((obj)=>{
            return worldObject.id != obj.id;
        });
    }

}

// create a test room
var testRoom = new Room();
testRoom.setID(uuidv1());
activeGameRooms.push(testRoom);

var testRoom2 = new Room();
testRoom2.setID(uuidv1());
activeGameRooms.push(testRoom2);

var arenas = io
    .of("arenas")
    .on('connection', function (socket) {
        var CurrUser = new User();
            CurrUser.playerName = "test";
            CurrUser.activity = ACTIVITIES.LOBBY;
            CurrUser.id = socket.id;
        var CurrActiveRoom = null

        activeUsers.push(CurrUser);

        // Emit to new socket client the active game rooms
        socket.emit('openRooms', {
            games: activeGameRooms
        })

        // On user disconnect, remove them from a room, and the active users list.
        socket.on('disconnect', ()=>{
            var userIdx = activeUsers.findIndex((user)=>{
                user.id == CurrUser.id;
            });

            if(CurrActiveRoom !== null) {
                CurrActiveRoom.removeUser(CurrUser);
                CurrActiveRoom.emitUpdate();
            }

            activeUsers.splice(userIdx, 1);
        })

        // Join a user to a room
        socket.on('joinRoom', (joinRequest, cb)=>{
            var room = joinRequest.room;
            var foundRoomIdx = activeGameRooms.findIndex((subRoom)=>{
                return subRoom.id === room.id;
            });
            var hasRoom = foundRoomIdx >= 0;

            if (hasRoom) {
                CurrUser.activity = ACTIVITIES.INGAME;
                CurrUser.roomId = activeGameRooms[foundRoomIdx].id;
                
                CurrActiveRoom = activeGameRooms[foundRoomIdx]
                CurrActiveRoom.addUser(CurrUser);

                socket.join(CurrUser.roomId);
                CurrActiveRoom.emitUpdate();

                cb(true);
            } else {
                cb(false);
            }
        })


        socket.on('addObject', (data, cb)=>{
            console.log("addObject", data, typeof data);
            if(typeof data != "object")
                return false;
            var newObj = CurrActiveRoom.addWorldObject(data);
            cb(newObj);
        });

        // objectUpdate is called when a game sprite has 
        // a change to tell the server, io payload:
        // {
        //  'objectId',
        //  'data' // will be forwarded straight on to client
        // }
        // note: this is for quick updates.
        socket.on('objectUpdate', (data)=>{
            // console.log('objectUpdate', CurrUser.roomId, data);
            arenas.in(CurrUser.roomId).emit('objectUpdate', data);
        });

    });